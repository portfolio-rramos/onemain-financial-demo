FROM node:17.1.0

WORKDIR /app

COPY ./dist ./dist
COPY ./proxy.conf.js ./proxy.conf.js
COPY ./express.js ./express.js

RUN npm install express
RUN npm install http-proxy-middleware
RUN npm install compression


EXPOSE 3000

CMD [ "node", "express"]


