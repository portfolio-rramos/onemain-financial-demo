/* SystemJS module definition */

declare var module: NodeModule;

interface NodeModule {
  id: string;
}

declare global {
  interface Window {
    Apollo: any;
  }
}
/*
 * Custom Type Definitions
 * When including 3rd party modules you also need to include the type definition for the module
 * if they don't provide one within the module. You can try to install it with @types

npm install @types/node
npm install @types/lodash

 * If you can't find the type definition in the registry we can make an ambient/global definition in
 * this file for now. For example

declare module 'my-module' {
 export function doesSomething(value: string): string;
}

 * If you are using a CommonJS module that is using module.exports then you will have to write your
 * types using export = yourObjectOrFunction with a namespace above it
 * notice how we have to create a namespace that is equal to the function we're
 * assigning the export to

declare module 'jwt-decode' {
  function jwtDecode(token: string): any;
  namespace jwtDecode {}
  export = jwtDecode;
}


 *
 * If you're importing a module that uses Node.js modules which are CommonJS you need to import as
 * in the files such as main.browser.ts or any file within app/
 *

import * as _ from 'lodash'

 * You can include your type definitions in this file until you create one for the @types
 *
 */

declare let assert: any;
declare let _: any;

/*
// for legacy tslint etc to understand rename 'modern-lru' with your package
// then comment out `declare module '*';`. For each new module copy/paste
// this method of creating an `any` module type definition
declare module 'modern-lru' {
  let x: any;
  export = x;
}
*/

// declare let $: any;
interface JQuery {
  dialog(options?: any, callback?: () => {}): any;

  datepicker(...args): any;

  select2(...args): any;

  clockpicker(...args): any;
}

interface Object {
  assign(...options: any[]): any;
}

declare module '*.json' {
  const value: any;
  export default value;
}

interface String {
  endsWith(searchString: string, endPosition?: number): boolean;
}

// Extra variables that live on Global that will be replaced by webpack DefinePlugin
declare let ENV: string;
declare let USE_MOCKDATA: boolean;
declare let BUILD_ON: string;
declare let REVISION: string;
declare let VERSION: string;
declare let PROXY: boolean;
declare let HMR: boolean;
declare let System: SystemJS;

interface SystemJS {
  import: (path?: string) => Promise<any>;
}

interface Window {
  saveSettings: () => void;
  resetSettings: () => void;
  classHolder: Element;
}

interface GlobalEnvironment {
  ENV: string;
  HMR: boolean;
  SystemJS: SystemJS;
  System: SystemJS;
}

interface Es6PromiseLoader {
  (id: string): (exportName?: string) => Promise<any>;
}

type FactoryEs6PromiseLoader = () => Es6PromiseLoader;
type FactoryPromise = () => Promise<any>;

type AsyncRoutes = {
  [component: string]:
    | Es6PromiseLoader
    | Function
    | FactoryEs6PromiseLoader
    | FactoryPromise;
};

type IdleCallbacks =
  | Es6PromiseLoader
  | Function
  | FactoryEs6PromiseLoader
  | FactoryPromise;

interface WebpackModule {
  hot: {
    data?: any;
    idle: any;
    accept(
      dependencies?: string | string[],
      callback?: (updatedDependencies?: any) => void,
    ): void;
    decline(deps?: any | string | string[]): void;
    dispose(callback?: (data?: any) => void): void;
    addDisposeHandler(callback?: (data?: any) => void): void;
    removeDisposeHandler(callback?: (data?: any) => void): void;
    check(
      autoApply?: any,
      callback?: (err?: Error, outdatedModules?: any[]) => void,
    ): void;
    apply(
      options?: any,
      callback?: (err?: Error, outdatedModules?: any[]) => void,
    ): void;
    status(callback?: (status?: string) => void): void | string;
    removeStatusHandler(callback?: (status?: string) => void): void;
  };
}

interface WebpackRequire {
  (id: string): any;

  (paths: string[], callback: (...modules: any[]) => void): void;

  ensure(
    ids: string[],
    callback: (req: WebpackRequire) => void,
    chunkName?: string,
  ): void;

  context(
    directory: string,
    useSubDirectories?: boolean,
    regExp?: RegExp,
  ): WebpackContext;
}

interface WebpackContext extends WebpackRequire {
  keys(): string[];
}

interface ErrorStackTraceLimit {
  stackTraceLimit: number;
}

// Extend typings
interface NodeRequire extends WebpackRequire {}

interface ErrorConstructor extends ErrorStackTraceLimit {}

interface NodeRequireFunction extends Es6PromiseLoader {}

interface NodeModule extends WebpackModule {}

interface Global extends GlobalEnvironment {}

// https://stackoverflow.com/questions/41285211/overriding-interface-property-type-defined-in-typescript-d-ts-file/74216680#74216680

type ModifyDeep<A, B extends DeepPartialAny<A>> = {
  [K in keyof A | keyof B]: K extends keyof A
    ? K extends keyof B
      ? A[K] extends AnyObject
        ? B[K] extends AnyObject
          ? ModifyDeep<A[K], B[K]>
          : B[K]
        : B[K]
      : A[K]
    : B[K];
};

type AnyObject = Record<string, any>;

type DeepPartialAny<T> = {
  /** Makes each property optional and turns each leaf property into any, allowing for type overrides by narrowing any. */
  [P in keyof T]?: T[P] extends AnyObject ? DeepPartialAny<T[P]> : any;
};
