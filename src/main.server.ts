import { enableProdMode } from '@angular/core';
import { isProduction } from './environments';

if (isProduction) enableProdMode();

export { AppServerModule as default } from './app/app-server.module';
