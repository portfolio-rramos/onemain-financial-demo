import { ENVIRONMENT } from './revision';
export const V1_API = '/api';
export * from './revision';
export const isProduction = ENVIRONMENT === 'production';
