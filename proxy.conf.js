const V1_ADDRESS = process.env.V1_ADDRESS || "http://localhost:9888";
const v1Target = `${V1_ADDRESS}`;

module.exports = [
  {
    context: ["/api/"],
    target: v1Target,
    secure: false,
    changeOrigin: true,
    pathRewrite: {
      "^/api/": "/",
    },
  },
  {
    context: ["/graphql"],
    target: v1Target,
    secure: false,
    changeOrigin: true,
    pathRewrite: {
      "^/graphql/": "/graphql",
    },
  },
];
