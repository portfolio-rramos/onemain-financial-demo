#!/bin/bash
set -e


VERSION=$(awk -F'"' '/"version": ".+"/{ print $4; exit; }' ./package.json)

REGISTRY='registry.gitlab.com/portfolio-rramos/onemain-financial-demo';
TAG_LATEST="${REGISTRY}:latest";
TAG_COMMIT="${REGISTRY}:${VERSION}";

members=(
    "REGISTRY_PATH=${TAG_LATEST}"
    "VERDACCIO_AUTH_TOKEN=${VERDACCIO_AUTH_TOKEN}"
)

COMMAND="docker buildx build -t ${TAG_COMMIT} -t ${TAG_LATEST}"
for str in "${members[@]}"; do
  COMMAND+=" --build-arg ${str}"
done

eval "cat ~/gitlab-token.txt | docker login registry.gitlab.com -u ${USERNAME} --password-stdin"
eval "npm run build"
eval "${COMMAND}  --no-cache ."
eval "docker push ${TAG_LATEST}"
eval "docker push ${TAG_COMMIT}"



