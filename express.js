/**
 * Created by ramor11 on 9/8/2016.
 */

const port = 3000;

const express = require("express");
const path = require("path");
const { createProxyMiddleware } = require("http-proxy-middleware");
const compression = require("compression");
const app = express();
const proxyConfig = require("./proxy.conf.js");

app.use(compression());

//http://evanhahn.com/express-dot-static-deep-dive/
const maxAge = 604800; // 1 week milliseconds
app.use(
  express.static(path.join(__dirname, "dist"), {
    etag: false,
    maxAge,
  }),
);

proxyConfig.forEach((k) => {
  const context = k["context"];
  const options = Object.assign({}, k);
  delete options["context"];
  app.use("/", createProxyMiddleware(context, options));
});

app.get("*", (req, res) =>
  res.sendFile(path.join(__dirname, "dist/index.html")),
);
app.listen(port, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log(`express =>  http://localhost:${port}`);
  }
});
