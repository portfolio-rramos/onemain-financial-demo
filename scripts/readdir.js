const fs = require("fs");
const helpers = require("./helpers");

const path = helpers.root(`src/assets/i18n/`);
const languages = JSON.parse(
  fs.readFileSync(
    helpers.root(`src/assets/i18n/supported_languages.json`),
    "utf8",
  ),
);

fs.readdir(path, function (err, items) {
  for (let i = 0; i < items.length; i++) {
    const name = items[i].split(".")[0];
    const descriptions = languages.find((o) => o.language === name);
    if (descriptions) console.log(descriptions);
  }
});
