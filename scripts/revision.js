const { writeFile, readFile } = require("fs");
const { argv } = require("process");
const helpers = require("./helpers");
let revision = argv[2] || "local";
const targetPath = `./src/revision.ts`;
const proxi = require(helpers.root("proxy.conf.js"));
const pkg = require(helpers.root("package.json"));

const REVISION = 'demo';
const VERSION = pkg.version;

function buildEnvironment(bool) {
  const results = `
export const ENVIRONMENT:string = '${bool ? "production" : "development"}';
export const REVISION:string = '${REVISION}';
export const VERSION:string = '${VERSION}';
`;

  return results;
}

function getEnvConfigFile(env) {
  switch (env) {
    case "production":
      return buildEnvironment(true);

    default:
      return buildEnvironment(false);
  }
}

console.log("environment", revision);

/* tslint:disable:no-console */
writeFile(targetPath, getEnvConfigFile(revision), (err) => {
  if (err) console.log(err);

  console.log(
    "\x1b[33m%s\x1b[0m",
    `================================================`,
  );
  console.log(
    "\x1b[33m%s\x1b[0m",
    `PROXY: ${JSON.stringify(proxi)}`,
    "\x1b[0m",
  );
  console.log(
    "\x1b[33m%s\x1b[0m",
    `================================================\n`,
  );
  console.log(
    "\x1b[33m%s\x1b[0m",
    `${revision}: Output generated at ${targetPath}`,
    "\x1b[0m",
  );
});

exports.buildEnvironment = buildEnvironment;
