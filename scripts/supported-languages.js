const fs = require("fs");
const helpers = require("./helpers");
const API_KEY_FILE = helpers.root("../google_translate_api.txt");
const API_KEY = fs.readFileSync(API_KEY_FILE, "utf8").toString().split("\n")[0];
const googleTranslate = require("google-translate")(API_KEY);

const writeToDirectory = helpers.root(
  `src/assets/i18n/supported_languages.json`,
);

googleTranslate.getSupportedLanguages("en", function (err, languageCodes) {
  fs.writeFile(writeToDirectory, JSON.stringify(languageCodes), function (err) {
    if (err) throw err;
    console.log("FILE GENERATED:", writeToDirectory);
  });
});
