const fs = require("fs");
const helpers = require("./helpers");
const API_KEY_FILE = helpers.root("../google_translate_api.txt");
const API_KEY = fs.readFileSync(API_KEY_FILE, "utf8").toString().split("\n")[0];
const googleTranslate = require("google-translate")(API_KEY);
const Rx = require("rxjs");
//rewrite all the languages, avoid defaultLanguage
const REWRITE = true;
//supported languages
const defaultLanguage = "en-US";
//directory to write languages

const { argv } = require("process");
const directory = argv[2] || "";
const writeToDirectory = helpers.root(
  `src/assets/i18n${directory ? "/" + directory : ""}`,
);
const languages = JSON.parse(
  fs.readFileSync(
    helpers.root(`src/assets/i18n/supported_languages.json`),
    "utf8",
  ),
);
const defaultLanguageFile = JSON.parse(
  fs.readFileSync(`${writeToDirectory}/${defaultLanguage}.json`, "utf8"),
);

const languageCodes = languages.map((l) => l.language);

const isObject = (test) => typeof test === "object";
const isString = (test) => typeof test === "string";

async function translate(language, text, callback) {
  let string = text;
  return await new Promise((success) => {
    googleTranslate.translate(text, language, function (err, translation) {
      if (translation) string = translation.translatedText;
      callback(string);
      success(string);
    });
  });
}

async function LanguageGenerator(lang, path, count) {
  const observables = [];

  function loopThrough(obj) {
    const handler = {};
    Object.keys(obj).forEach((key) => {
      const test = obj[key];
      if (isString(test)) {
        const promise = translate(lang, test, (text) => (handler[key] = text));
        observables.push(promise);
      } else if (isObject(test)) handler[key] = loopThrough(test);
    });
    return handler;
  }

  const query = loopThrough(defaultLanguageFile);
  Promise.all(observables).then((data) => {
    fs.writeFile(path, JSON.stringify(query), function (err) {
      if (err) throw err;
      console.log("FILE GENERATED:", path);
      proceessLanguage(count + 1);
    });
  });
}

function proceessLanguage(index) {
  if (languageCodes[index]) {
    const l = languageCodes[index];
    //ovoid writing default language, it can get stuck in endless loop
    if (l !== defaultLanguage) {
      const path = `${writeToDirectory}/${l}.json`;
      if (REWRITE || !fs.existsSync(path)) {
        const lang = l.split("-")[0];
        LanguageGenerator(lang, path, index);
      }
    } else proceessLanguage(index + 1);
  }
}

proceessLanguage(0);
