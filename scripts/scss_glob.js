const { join } = require("path");
const { promises } = require("fs");
const { readdir, writeFile } = promises;
const helpers = require("./helpers");

const glob_directories = [
  "vendors/_modules/_app",
  "vendors/_extensions",
  "vendors/_modules/_keyframes",
  "vendors/_modules/_mod",
  "vendors/_modules/_page-components",
  "vendors/_modules/_settings",
];

const root_path = "/src/scss";

async function getFiles() {
  for (const dir of glob_directories) {
    const configDirectory = join(process.cwd(), root_path, dir);
    const files = await readdir(configDirectory);
    const destination = join(configDirectory, "_index.scss");
    const data = files
      .filter((a) => a !== "_index.scss")
      .map((f) => `@import '${f}';`)
      .join("\n");
    await writeFile(destination, data, "utf-8");
  }
}

getFiles().then();
