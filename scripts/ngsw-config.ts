const { writeFile } = require("fs");
const targetPath = `./src/ngsw-config.json`;

const ngswConfig = {
  index: "/index.html",
  assetGroups: [
    {
      name: "app",
      installMode: "prefetch",
      resources: {
        files: [
          "https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js",
          "/favicon.ico",
          "/index.html",
          "/*.css",
          "/*.js",
        ],
      },
    },
    {
      name: "assets",
      installMode: "lazy",
      updateMode: "prefetch",
      resources: {
        files: [
          "/assets/**",
          "/*.(svg|cur|jpg|jpeg|png|apng|webp|avif|gif|otf|ttf|woff|woff2)",
        ],
      },
    },
  ],
  dataGroups: [
    // {
    //   name: "file",
    //   urls: ["/v2/api/file/*", "/v1/api/sheets/*", "/v1/api/workbook/*"],
    //   // version: 1,
    //   cacheConfig: {
    //     strategy: "freshness",
    //     maxSize: "100000",
    //     maxAge: "12h",
    //     timeout: "10s",
    //   },
    // },
    {
      name: "equuocs",
      urls: [
        "/v1/api/i18n/welcome",
        "/v1/api/i18n/disclaimer",
        "/v1/api/i18n/features",
        "/v1/api/i18n/pricing",
        "/v1/api/i18n/about",
        "/v1/api/i18n/sources",
        "/v1/api/i18n/privacy-policy",
      ],
      cacheConfig: {
        maxSize: "100000",
        maxAge: "14d",
        timeout: "10s",
      },
    },
  ],
  navigationUrls: ["!/api/swagger/**/*.*"],
};

function buildNgswConfig() {
  return `${JSON.stringify(ngswConfig)}`;
}

/* tslint:disable:no-console */
writeFile(targetPath, buildNgswConfig(), (err) => {
  if (err) console.log(err);

  console.log(
    "\x1b[33m%s\x1b[0m",
    `================================================`,
  );
  console.log("\x1b[33m%s\x1b[0m", `write => ngsw-config.json`, "\x1b[0m");
  console.log(
    "\x1b[33m%s\x1b[0m",
    `================================================\n`,
  );
});

exports.buildNgswConfig = buildNgswConfig;
